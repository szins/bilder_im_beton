from PIL import Image
import math

cell_size = 21                                      # cell size for one pixel pattern (height & width)
image_name = "test_images/1.jpg"              # image to be transformed
ds_value = 0.2                                   # downscaling value -> 0.4 = 40% of the original size, needed for better computing time
saving = True                                       # determines if transformed image should be saved

# 3 = important <=> 0 = unimportant
# TODO (0) implementation for white stripes over seethrough background -> png
# TODO (3) dynamic downsizing for high resolution images -> processing time way too long

def change_contrast (image, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))
    def contrast(c):
        return 128 + factor * (c - 128)
    return image.point(contrast)

def partition(lst, count):
    if count == 3:
        return []
    numb = 0
    total = 0
    for i in range(256):
        numb += lst[i]
        total += i * lst[i]
    split_index = round(total / numb)
    lst2 = [0] * 256
    for i in range(split_index+1):
        lst2[i] = lst[i]
        lst[i] = 0
    a = partition(lst, count+1)
    b = partition(lst2, count+1)
    return [split_index] + a + b

def color_distribution(img):
    pixels = img.load()
    color = [0] * 256
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            index = pixels[i,j]
            color[index] += 1
    color = partition(color, 0)
    color.sort()
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            test = False
            for k in range(len(color)):
                if test == True:
                    continue
                if (pixels[i,j] < color[k]):
                    pixels[i,j] = color[k]
                    test = True
            if (test == False):
                pixels[i,j] = 256
    return color

def color_map(color, cs, color_code):   # cs = cell_size
    """
    maps a color to a corresponding pixel pattern dipending on the cell_size
    """

    if (len(color_code)==0):
        color_code = [100,120,140,160,180,200,220]

    # short lambda function for dividing a cell into the black and white parts
    # namely the x-index of the first element of every color block
    div = lambda x, cs: (round((cs-x)/2),x + round((cs-x)/2), cs)

    if (color < color_code[0]):
        return div(3,cs)
    elif (color < color_code[1]):
        return div(5,cs)
    elif (color < color_code[2]):
        return div(7,cs)
    elif (color < color_code[3]):
        return div(9,cs)
    elif (color < color_code[4]):
        return div(11,cs)
    elif (color < color_code[5]):
        return div(13,cs)
    elif (color < color_code[6]):
        return div(15,cs)
    else:
        return div(17,cs)

def btmp(cell_size, image):
    """
    creates new image, by applaying a pixel 
    pattern over each and every pixel of the original image
    """
    # loads pixelarray from original image
    pixels_orig = image.load()

    #color_code = color_distribution(image)
    color_code = []

    # assign dimensions of image to bitmap
    width, height = image.size
    dim0 = width
    dim1 = height

    # create new bitmap: L = Grayscale (single number values from 0-255), size = dimensions 
    new_img = Image.new(mode = "L", size = (dim0*cell_size, dim1*cell_size))
    # create pixel array corresponding to bitmap
    pixels = new_img.load()

    # loop through every pixel in orig image
    for i in range(dim0):
        for j in range(dim1):
            # loop cell_size - times in y direction
            for j1 in range(cell_size):
                # map color to color distribution
                col = color_map(pixels_orig[i,j], cell_size, color_code)
                # create pixel pattern in black-white-black, width of cell_size
                for i1 in range(col[0]):
                    pixels[i * cell_size + i1, j * cell_size + j1] = 0
                for i1 in range(col[0], col[1]):
                    pixels[i * cell_size + i1, j * cell_size + j1] = 255
                for i1 in range(col[1], col[2]):
                    pixels[i * cell_size + i1, j * cell_size + j1] = 0
    return new_img

# load image and 
orig = Image.open(image_name)
# increase contrast for better differntiation between colors
#orig = change_contrast(orig, 100)

# convert to ("L")=greyscale for simpler output
orig = orig.convert("L")
# resize image to reduce pixel number
width, height = orig.size
new_size = (round(width * ds_value), round(height * ds_value))
orig = orig.resize(new_size)

# convert image in bitmap with pixel pattern
ret_img = btmp(cell_size, orig)


if (saving):
    ret_img.save("test_images/test_fetsch.jpg")

# show image
ret_img.show()